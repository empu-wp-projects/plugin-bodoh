<?php
/**
 * Plugin Bodoh
 * 
 * @package Bodoh
 * @author Wuri Nugrahadi <wuri.nugrahadi@empu.co.id>
 * @copyright 2019 Wuri Nugrahadi, eMpu Datanetics
 * @license MIT
 * 
 * @wordpress-plugin
 * Plugin Name: PLUGIN BODOH
 * Plugin URI:  https://empu.co.id/code/plugin-bodoh
 * Description: Plugin percobaan, bukti kebodohan ga mau belajar WP
 * Version:     0.1.0
 * Author:      Wuri Nugrahadi
 * Author URI:  https://wuri.nugrahadi.com
 * Text Domain: plugin-bodoh
 * License:     MIT
 */